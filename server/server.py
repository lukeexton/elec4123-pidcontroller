#!/usr/bin/env python

# Server Side for Elec4123 Project
#
# Authored by Luke Exton

from bottle import route, run, hook, response
import json


current_temp = 20
set_temp = 45
power = 'on'


hostname = "0.0.0.0"
port = 8080


@hook('after_request')
def disable_cachine():
    response.headers["Cache-Control"] = 'public, max-age=0'
    response.headers["Accept"] = 'application/json'
    response.headers["Content-Type"] = 'application/json'


def get_data(param):
    with open('data.json', 'r') as f:
        data = json.load(f)
        return str(data[param])


def modify_data(param, value):
    j = ""
    with open('data.json', 'r') as f:
        j = json.load(f)

    j[param] = value

    with open('data.json', 'w') as f:
        json.dump(j, f)

    return str(get_data(param))


@route('/temp/current')
def get_temp():
    return get_data("current")


@route('/temp/set')
def get_set_temp():
    return get_data("set")


@route('/power')
def get_power():
    return get_data("power")


@route('/temp/set/<temp>')
def set_set_temp(temp):
    return modify_data("set", temp)


@route('/temp/current/<temp>')
def set_current_temp(temp):
    return modify_data("current", temp)


@route('/power/<state>')
def set_power_state(state):
    if state == 'on':
        return modify_data("power", 'on')
    elif state == 'off':
        return modify_data("power", 'off')
    else:
        response.status = 400
        return "err: Invalid Request, this should be either \"on\" or \"off\""


if __name__ == "__main__":
    dat = {}

    dat['current'] = str(current_temp)
    dat['set'] = str(set_temp)
    dat['power'] = str(power)

    with open('data.json', 'w') as f:
        json.dump(dat, f)

    exit(run(host=hostname, port=port))



