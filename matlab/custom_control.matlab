%
% ELEC4123
% PID Controller
%
% Luke Exton and Rob Joseph Ã‚Â© 2015
%
%

close all
clear all

% DAQ Constants
global VENDOR;
VENDOR = 'ni';
global DAQ_ID;
DAQ_ID = 'nidaq';

global DEVICE_NAME;
info = daqhwinfo(DAQ_ID);
if length(info.InstalledBoardIds)
DEVICE_NAME = info.InstalledBoardIds{1};
end

% IO Constants
global IN_CHANNEL;
IN_CHANNEL = 0;
global OUT_CHANNEL;
OUT_CHANNEL = 0;
global HIGH;
HIGH = 5;
global LOW;
LOW = 0;

% DAQ Sampling Details
global SAMPLE_RATE;
%SAMPLE_RATE = 2000;
SAMPLE_RATE = 80; %TEST!!!!!!

global SAMPLES_PER_TRIGGER
% Total Number of samples we will take per trigger
% There is nothing to suggest we need to change this number
% Since we will modify it as we need it. 
SAMPLES_PER_TRIGGER = 50; 

global seconds;
seconds = 300;

%MAGIC GOES HERE
global magic_seconds;
magic_seconds = 5;


% Intitialization of DAQ
ai = analoginput(DAQ_ID, DEVICE_NAME);
ao = analogoutput(DAQ_ID, DEVICE_NAME);
addchannel(ai, IN_CHANNEL);
addchannel(ao, OUT_CHANNEL);

ai.SampleRate = SAMPLE_RATE;
ai.SamplesPerTrigger = SAMPLES_PER_TRIGGER;
ai.TriggerType = 'manual';

start(ai)
trigger(ai)
wait(ai,10)

% This is the initial voltage
starting_voltage = mean(getdata(ai));


% cutoff_voltage = 6.000;
% if starting_voltage > cutoff_voltage
%     msgID = 'TooHot:Error';
%     msg = 'Temperature too hot.';
%     %disp(starting_voltage);
%     throw(MException(msgID,msg));
% end
    
% Step UP
%putsample(ao,HIGH);


% Iterate for n seconds
data = [];
data(1)=0;

% This will be the maximum voltage to be output by this method.
% This will be determined by the hardware itself. And should only be
% reconfigured with adjustment to the amplification circuit above.
U_MAX = 5;
U_MIN = 2;
setpoint=7.28;
Kp=3;
Ki=0.0763;
Kd=10;
x = [0,0,0];

% Two value circular buffer.
error = [0,0];
% Implement the control mechanism
controlling = true;

% First iteration of dt
dt = 1;

% Iterate for n seconds
data = [];
data(1)=0;
temp = [];
Time=[];
Time(1)=0;
output_plot=[];
output_plot(1)=0;
error_plot=[];
error_plot(1)=0;
i=0;

T=0;
while controlling && (T < seconds)
	tic
	start(ai)
	trigger(ai)
	wait(ai, 10)
   i=i+1;
    
	% Get a current sample
	acquiredData = getdata(ai);
    data(end+1) = mean(acquiredData(SAMPLES_PER_TRIGGER)); %#ok<AGROW>
    
	% Calculate the current error between the current data
	% We take the mean of this since we are getting 50 points of data per
	error(1) = setpoint - mean(data(end));
    disp('e:');
    disp(error(1));
    % Proportional
	x(1) = error(1);
	% Integral
	x(2) = x(2) + error(1) * dt;
	% Derivative
	x(3) = ( error(1) - error(2) ) / dt;
    
	output = Kp * x(1) + Ki * x(2) + Kd * x(3);
    
    error(2) = error(1); % We can set this now that we are done with it.
    
	% Clipping design - No input greater than what the driver can handle
	% for the motor/generator combo
% 	if output > U_MAX
%    		output = U_MAX;  
%     end
%     
%     if output < U_MIN
%    		output = U_MIN;  
%     end
    

    %Clip output if it tries to go negative
%     if output < 0
%         disp('going negative')
%    		output = 0;  
%     end
    
    %Display for debugging purposes
    %disp(T)
    %disp(error(1))
    %disp(data(end))
    disp('o:')
    disp(output)
    
    % Display data in real-time
    Time(i)=i/magic_seconds;
    figure(1)
	subplot(4,1,1)
	temp(end+1) = 2.929*data(end) + 23.674;
	plot(Time(1:i),temp(1:i));
    xlabel('time (sec)');
    ylabel('temperature(degrees Celsius)');
    title('controlled response');
    subplot(4,1,2)
    plot(Time(1:i),data(1:i));
    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('controlled response');
    subplot(4,1,3)
    output_plot(end+1)=output;
    plot(Time(1:i),output_plot(1:i));
    xlabel('time (sec)');
    ylabel('PID output(V)');
    title('controlled response');
    subplot(4,1,4)
    error_plot(end+1)=error(1);
    plot(Time(1:i),error_plot(1:i));
    xlabel('time (sec)');
    ylabel('PID Input Error(V)');
    title('Error Plot');
    
    
    drawnow
    
    putsample(ao, output);
 	dt = toc;
    T=T+toc;
end   
putsample(ao,LOW);

%Commented out function bookends because wasn't executing
%function plot_data (data)
    %function plot_data (data, smooth_data)
    figure(2)
    time = T/numel(data)*(1:(numel(data)));
    %plot(time, data, time, smooth_data)
    plot(time, data);

    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('step response');
%end


%end
