function [set_temp] = get_set_temp( hostname )
	try
    	temp=webread(strcat('http://', hostname, '/temp/set');
    	res = str2num(temp);
    catch
    	res = set_temp;
    end
    return res;
end