function [power_on] = should_power_be_on( hostname )
    % Will return a boolean based on the power varaible
    try
    	power_state = webread(strcat('http://', hostname, '/power'));
    	res = strcmp(power_state,'on');
    catch
    	res = 1; % This is the same as when the strcmp is true
    return res;
end