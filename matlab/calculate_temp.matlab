%
% ELEC4123
% Convert the Voltage to a Temperature
%
% Luke Exton and Rob Joseph © 2015
%
%


function [t] = calc_temp(voltage_input, r1, V_in)
    % CALC_TEMP calculate the temperature from a single number based based on the r1.
    %   voltage_input = single number to convert to temperature
    %   r1 = resistor to use in the calculation value
    %   V_in =(optional) input regarding the 


    if nargin > 3
    error('control:calc_temp:TooManyInputs', ...
        'requires at most 1 optional inputs');
    end

    switch nargin
        case 6
            V_in = 12;
    end


	% Input protections
	voltage = '';

    % From the datasheet
    RES_ARRAY = [32554, 25339, 19872, 15698, 12488, 10000, 8059, 6535, 5330, 4372, 3605 ];

	try
    	% Accept an array as an input
    	voltage = mean(voltage_input);
	catch
    	% Accept a single digit as input
    	if ISA(voltage,'number');
        	voltage = voltage_input;
    	else
        	throw(baseException);
    	end
	end

	v_array = [];

	% Recalcuate voltage levels
	for r2 = RES_ARRAY
    	tmp_v = V_in * r2 / (r1 * r2);
    	v_array(end+1) = tmp_v;
	end
    
	%Temperature Calculation returns t
	index = 0;
	t = 0;
	for v = v_array
    	index+=1
    	if voltage > v
        	v_diff = voltage - (v_array[index] / (v_array[index+1] - v_array[index]));
        	t = TEMP_ARRAY[index] + v_diff = (TEMP_ARRAY[index+1] - TEMP_ARRAY[index]);
        	clearvars v_diff  index;
        	break
    	end
	end
end