%
% ELEC4123
% Transfer Function Generator
%
% Luke Exton and Rob Joseph © 2015
%
%

%function [data] = generateTransferFunction

close all
clear all

% DAQ Constants
global VENDOR;
VENDOR = 'ni';
global DAQ_ID;
DAQ_ID = 'nidaq';

global DEVICE_NAME;
info = daqhwinfo(DAQ_ID);
if length(info.InstalledBoardIds)
DEVICE_NAME = info.InstalledBoardIds{1};
end

% IO Constants
global IN_CHANNEL;
IN_CHANNEL = 0;
global OUT_CHANNEL;
OUT_CHANNEL = 0;
global HIGH;
HIGH = 3;
global LOW;
LOW = 0;

% DAQ Sampling Details
global SAMPLE_RATE;
SAMPLE_RATE = 2000;

global SAMPLES_PER_TRIGGER
% Total Number of samples we will take per trigger
% There is nothing to suggest we need to change this number
% Since we will modify it as we need it. 
SAMPLES_PER_TRIGGER = 50; 

global seconds;
seconds = 800;

%MAGIC GOES HERE
global magic_seconds;
magic_seconds = 5;


% Intitialization of DAQ
ai = analoginput(DAQ_ID, DEVICE_NAME);
ao = analogoutput(DAQ_ID, DEVICE_NAME);
addchannel(ai, IN_CHANNEL);
addchannel(ao, OUT_CHANNEL);

ai.SampleRate = SAMPLE_RATE;
ai.SamplesPerTrigger = SAMPLES_PER_TRIGGER;
ai.TriggerType = 'manual';

start(ai)
trigger(ai)
wait(ai,10)

% This is the initial voltage
starting_voltage = mean(getdata(ai));


cutoff_voltage = 6.000;
if starting_voltage > cutoff_voltage
    msgID = 'TooHot:Error';
    msg = 'Temperature too hot.';
    disp(starting_voltage);
    throw(MException(msgID,msg));
end
    
% Step UP
putsample(ao,HIGH);


% Iterate for n seconds
data = [];
data(1)=0;
temp = [];
Time=[];
i = 0;
tic

while ((i < magic_seconds * seconds) && (data(end)<9.8)) %End safe & early
%while (i < magic_seconds * seconds)
    i = i + 1;
    start(ai)
    trigger(ai)
    wait(ai,10)
    acquiredData = getdata(ai);
    data(end+1) = mean(acquiredData(SAMPLES_PER_TRIGGER));
    %Display time & data for debugging purposes
    disp(i/magic_seconds)
    disp(data(end))
    % Display data in real-time
    Time(i)=i/magic_seconds;
    figure(1)
	subplot(2,1,1)
	temp(end+1) = 2.929*data(end+1) + 23.674
	plot(Time(1:i),temp(1:i));
    subplot(2,1,2)
    plot(Time(1:i),data(1:i));
    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('step response');
    drawnow
end
T = toc;
putsample(ao,LOW);
disp('Loop over');

%function smooth_data = smooth_data(data)
    % Filter out the data that we are processing
    % This might help with the reduction of noise
    % It is noted that this wasn't as neccisary once we
    % Decoupled the power supply and had a low-noise
    % Segment of the breadboard

    filter_order = 1;
    cutoff = 25;
     [b,a]=butter(filter_order,2*cutoff/SAMPLE_RATE);
     smooth_data = filter(b,a,data);

%end

%Commented out function bookends because wasn't executing
%function plot_data (data)
    %function plot_data (data, smooth_data)
    figure(2)
    time = T/numel(data)*(1:(numel(data)));
    %plot(time, data, time, smooth_data)
    plot(time, data);

    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('step response');
%end

%function crossing_point = calculate_three_db_point(data)
    % Analyize the data set to calculate the 3db sample point.
    % Currently assumes we have stability for 100 samples at least.

    % Constants
    stabilized_samples = 100;
    stabilized_start = 5;
    three_db = 0.632;

    % Measured Data
    final_voltage = mean(data(end-stabilized_samples:end));
     starting_voltage = data(stabilized_start);

     voltage_diff = final_voltage - starting_voltage;
    crossing_point = starting_voltage + (three_db * voltage_diff);

%end

%function tau = calculate_tau(data, crossing_point)
    % Identify the time in seconds of the crossing point.
    % This consumes the magic number to convert the 
    % sampling rate back to seconds rather than per sample.

    tau = 0;
    counter = 0;
    for v = data
        counter = counter + 1;
        if v > crossing_point
            tau = counter;
            break
        end
    end
    % Magic Conversion of the samples to time
    tau = tau / magic_seconds;
    
%     % It is important that tau is a double
%     % since we will consume tau later to calculate our a and k values
%     tau = double(tau);
%end


%x_point = calculate_three_db_point(data);
%tau = calculate_tau(data,x_point);
% 
 a = 1/tau;
 k = a * voltage_diff;
 s = tf('s');
% 
 % Since we are assuming single order
 % Validate against the measured step response.
 G = k / (s + a);
 H = (G + starting_voltage)/HIGH;
% 
% %Debugging
% disp(H)
% 
 % Make sure we plot this on a different figure to the previous
 figure(3);
 t=(0:1:1000);
 u=3*ones(size(t));
 lsim(H,u,t);
 
%end