//
//  ViewController.h
//  dac_thermo
//
//  Created by Luke Exton on 13/08/2015.
//  Copyright (c) 2015 Luke Exton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UILabel * setpoint;
    IBOutlet UILabel * current_temp;
    IBOutlet UIProgressView * current_temp_bar;
}
@property (weak, nonatomic) IBOutlet UITextField *hostname_field;

@end

