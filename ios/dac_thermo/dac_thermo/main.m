//
//  main.m
//  dac_thermo
//
//  Created by Luke Exton on 13/08/2015.
//  Copyright (c) 2015 Luke Exton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
