//
//  Sensor.m
//  dac_thermo
//
//  Created by Luke Exton on 14/08/2015.
//  Copyright (c) 2015 Luke Exton. All rights reserved.
//

#import "Sensor.h"

@implementation Sensor

@synthesize setpoint = _setpoint;
@synthesize current_temp = _current_temp;
@synthesize state = _state;
@synthesize hostname;

static Sensor* _sensor = nil;


+(Sensor*) sensor
{
    @synchronized([Sensor class])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"
        if (!_sensor)
            [[self alloc] init];
        return _sensor;
    }
#pragma clang diagnostic pop
}

+(id) alloc{
    @synchronized([Sensor class])
    {
        NSAssert(_sensor == nil, @"Attempted to allocate a second instance of Sensor.");
        _sensor = [super alloc];
        return _sensor;
    }
}

-(id) init {
    self = [super init];
    if (self != nil){
        [self setHostname:@"http://localhost:8080"];
        [self setSetpoint:45];
        [self current_temp];
        [self setState:@"on"];
    }
    return self;
}

-(NSInteger) setpoint {
    return _setpoint;
}

-(NSInteger) current_temp {
    _current_temp = [[self get:@"temp/current"] intValue];
    return _current_temp;
}

-(NSString * ) state {
    return _state;
}

-(void) setSetpoint:(NSInteger)setpoint{
    _setpoint = [[self get:[NSString stringWithFormat:@"temp/set/%ld", (long)setpoint ]] intValue];
}

-(void) setState:(NSString *)state{
    _state = [self get:[NSString stringWithFormat:@"power/%@", state ]];
}

-(NSString *) get:(NSString *) endpoint{
    return [self getDataFrom:[self generate_url:endpoint]];
}

-(NSString *) generate_url:(NSString *) endpoint{
    NSString * url = [NSString stringWithFormat: @"%@/%@",self.hostname, endpoint];
    return url;
}

- (NSString *) getDataFrom:(NSString *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %li", url, (long)[responseCode statusCode]);
        return nil;
    }
    
    return [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
}

@end
