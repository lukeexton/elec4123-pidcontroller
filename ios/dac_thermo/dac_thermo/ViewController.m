//
//  ViewController.m
//  dac_thermo
//
//  Created by Luke Exton on 13/08/2015.
//  Copyright (c) 2015 Luke Exton. All rights reserved.
//

#import "ViewController.h"
#import "Sensor.h"

@interface ViewController ()

@end



@implementation ViewController
@synthesize hostname_field;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self background_update_of_sensor];
    hostname_field.text = [[Sensor sensor] hostname];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"You entered %@",self.hostname_field.text);
    [[Sensor sensor] setHostname:hostname_field.text];
    [self.hostname_field resignFirstResponder];
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) updateSetpoint:(UISlider *) sender{
    // Cast to an int and round to the nearest whole number.
    NSInteger val = (int) sender.value + 0.5;
    setpoint.text = [NSString stringWithFormat:@"%ld °C",  (long)val];
    [[Sensor sensor] setSetpoint: val];
    
}

-(IBAction) updateState:(UISwitch *) sender{
    if ([sender isOn]){
        [[Sensor sensor] setState:@"on"];
        NSLog(@"Setting State to ON");
    } else {
        [[Sensor sensor] setState:@"off"];
        NSLog(@"Setting State to OFF");
    }
        
}

-(void) background_update_of_sensor{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^ {
        double expected_max_temp = 50.0f;
        while (true){
            NSInteger temp = [[Sensor sensor] current_temp];
            double progress = temp/expected_max_temp;
            NSLog(@"Temperature Reading: %ld, Progress: %.2f", (long)temp, progress);
            dispatch_async(dispatch_get_main_queue(), ^{
                current_temp_bar.progress = progress;
                current_temp.text = [NSString stringWithFormat:@"%.2ld °C", (long)temp ];
            });
            [NSThread sleepForTimeInterval:2.0f];
        }
                       
    });
}

-(IBAction) updateHostname:(UITextField * ) textfield{
    NSLog(@"New Hostname %@", textfield.text);
    [[Sensor sensor] setHostname:textfield.text];
}

@end
