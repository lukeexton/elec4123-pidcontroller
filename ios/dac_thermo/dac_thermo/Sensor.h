//
//  Sensor.h
//  dac_thermo
//
//  Created by Luke Exton on 14/08/2015.
//  Copyright (c) 2015 Luke Exton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sensor : NSObject{
    
}

// Singleton Instance
+(Sensor*)sensor;

@property (nonatomic) NSInteger setpoint;
@property (nonatomic) NSInteger current_temp;
@property (nonatomic) NSString * state;
@property (nonatomic) NSString * hostname;

@end
