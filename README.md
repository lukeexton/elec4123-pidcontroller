All our project resources can be found at the following URL. It contains context and the exact content for the project. For any further on the additional information, or the modules that are used to extend the project please see:

https://bitbucket.org/lukeexton/elec4123-pidcontroller

# Introduction & Design Requirements

## Design Brief

Calculate the plant model and build a controller

Starting from room temp, the output temperature should rise to 45 degrees.

Use Matlab to display controlled and uncontrolled output

Design Metrics:

- There should be no steady state error
- There should be no overshoot
- The rise time should be minimized

### Hard Requirements

- No More than 9V input to the Control Signal to drive the input voltage to the motor and generator combo.
- The output temperature should not exceed 45 degrees Celsius
- Starting from room temperature 25 degrees Celsius

### Potential Applications

When we consider the potential applications that this system could have it is important because we will need to select the paramaters that are the most likely to change. We are assuming that the system reflects a larger HVAC system, where the rise time isn't as importnat but the steady state fluctuation is more important.

Some potential applications are:

- HVAC Systems
- Boiler/Hot Water System (coffee machine/boiler)


## Components
- DAQ
- Matlab (2012R2)
- Data Acquisition Toolbox
- Motor Generator Combo (according to data-sheet)
- Infrared Thermometer (Artech A350)
- Driver (according to data-sheet)
- Thermistor NTCLE203E3 -> 10K Variation
- Standard Electronics

# Design
## Plant Model

As part of the design requirements, we had to model the plant (i.e. the driver, motor, generator, load, and thermistor).  A cursory examination of these components showed that our plant model would obviously not be a first-order system.  However, before embarking upon an exhaustive measurement of each of the physical parameters in the system, we decided to first test the plant with a step function.  Such an approach involves using the response to calculate the DC gain, poles, and zeros of the system.  By treating the whole plant as a ‘black box’, we could see if it was possible to approximate the transfer function to a simpler model.  Such an approach could save us time experimenting, leaving more time for design and analysis.

Inputting a 3V step into the system yielded the output curve below (thick line).  The second line is the approximated first-order system.

!["Steady State Response"] [step_response_data_points]
 
To obtain this approximated model, we first took three data points from the step response output: Vi, the initial voltage, tau, the time at which the response reaches 63.2% of its final value, and Vf, the final voltage.  From these, we could calculate the parameters a = 1/tau, and K = a * (final voltage – initial voltage).  The resulting first-order transfer function would then be of the form:
G(s)=K/(s+a)+starting voltage
Because our step was 3V, it was necessary to divide this by 3 to obtain our normalised transfer function.  Thus, with values of Vi = 0.5589V, tau = 149.1, a = 0.0067, Vf = 3.75, and K = 0.0214, we obtained the transfer function:
G_N (s)=0.0214/(s+0.0067)+0.5589

!["Transfer Function"][tf]

As can be seen in the plot above, when we plotted the 3V step response of this transfer function, it was a reasonable approximation of our actual plant model.  We believe it was a good enough approximation for the ranges of voltages we expected, and that the limitations of the approximation could be corrected with further PID tuning.

For the code used to implement the step response and calculate the transfer function, see the appendix "generateTransferFunction.m"

## Modules
### PID Module
The PID Controller is the core of the brains for the controller. It maintains a very small amount of state and can be controlled via the three parameters (Kp, Ki, Kd). 

This module will also look at the difference between the set-point and the expected value in order to control it. Our control block was manually implemented rather than using the PID function.

    error(1) = mean(data) - setpoint

    % Proportional
    x(1) = error(1);
    % Integral
    x(2) = x(2) + error(1) * dt;
    % Derivative
    x(3) = ( error(1) - error(2) ) / dt;
    
    output = Kp * x(1) + Ki * x(2) + Kd * x(3);

#### PID Tuning

Manual tuning was completed of the PID function. We looked into alternatives such as Ziegler–Nichols and the Matlab internal tuning.


We used the PIDtune in Matlab to get started and determine the ratios that we would use for the practical implementation to get started.


The final variables that we reached after around 20 iterations was Kp = 2, Ki = 0.001, Kd = 10. The main factor that limied the number of interations that could be compled was due to the 15 minute settling time back to room temperature.



### Sensor Module
The sensor module is implemented mostly in electronics, with a single function in order to convert the measured sensor data to a temperature. See more info in Thermistor Calibration for out calibration methodology. For the voltage that we selected for the R2 value of the sensor we chose 1K as it was the most suitable for the full temperature range. We generated an appropriate range based on a maximum output of 10V and a minimum output of 1V. This left us only with resistors in the range 1K -> 4K. The excel spreadsheet for these results electronics/R2_selector.xlsx


### Drive Module
The input to the drive module will come from the breadboard power supply. This has caused a few issues where the input power supply would be modified based on the current that is being drawn by the motor. This was mitigated in the Power Supply Decoupling Section.

# Design Considerations
## Thermistor Calibration
The calibration of the thermistor proved to be quite a challenge as thermo coupling between the thermistor and the load with imperfect. There is a transfer function that was impossible to measure without external tooling and manual intervention.

Our temperature algorithms depended not so much on the data from the data sheet, but experimental data mapping the temperature to the voltage drop across the thermistor.

We used the exposed side of the load as the target for the electrical sensor and assumed that at the surface was the most important detail. See the figure for the experimental data.

!["Experimental Procedure for Temperature Calibration"][temp_cali]


## Analogue vs Digital Signal
Initially we planned on implementing the control circuitry in analogue electronics using op amps. We completed an initial schematic which would take into consideration all the different modules required.

!["Initial Circuit Diagram"] [circuit]

Due to the scope of the project and ignoring cost efficiency, since we are only producing one unit, we decided against designing and implementing in analogue electronics and instre opted for digital implementation in Matlab.

## Power Supply Decoupling
We needed to drive a motor, as we will be powering this from the same power supply that we source the sensitive output voltage, we will need to remove any power supply variation.
We will decouple the power supply on the sensitive section of the breadboard by adding a choke and decoupling capacitor so that we have a separate segement of the breadboard that will be sensitive to electrical signals, and will have power supply noise limited as much as possible. 

# Test Plans

Our test plan has got the electronics that will vary minimized as much as possible. Our testing are veriification will be mostly completed and automated in software, for instance the selector for the connected development board will be automated.

Our Setup:
!["Breadboard and Experimental Setup"][lab]


## Setup Validation
- Verify the supply voltages are correct. Using a combination of LED's and a voltmeter
- Verify the connection of the motor and generator
- Verify the input and the output of the DAQ
- Validate that the supply voltage across the output sensor

## Procedure
- Run the generator_transfer_function.m
- Run cooldown.m
- Tune the Kp, Ki, Kd values
- Iterate as required
- Run pid_control.m, this will implement a PID with the values that are calculated from the PID Controller

note: (optional) if we update the setpoint, this should allow the output temperature to be dynamically configured as required, this would mirror the output signals.


# Performance

![Experimental Data][demo_steady_state_zoomed_marked]

We determined the maximum rise time to be around 100 seconds. When we maximized the input voltage (from 25 degrees to 45 degrees), here we note that room temperature is arbitrary, and for a lot of other engineering teams room temperature means 30-35 degrees. 

In order to tune the PID controller module, we set a fixed the Kp value (generated from the matlab PIDtune function), and then added a small element of Ki. This would mean that we should have no steady state error regardless. The steady state error at this stage is limited by the physical underlying hardware and physical implementation.

From this point we increased the KD value to minimize the overshoot (ideally nothing). However with large values and due to the changes that caused power supply fluctuations, disturbing the output signal, we found that it clouded the output signal.

We included a Ki proponent (very small, so there will be no steady state error).

Our Matlab code plots the output samples live, We have a Matlab function to pass the data through to a live server that can be found in our source code.

In the images directory we have included a few of the discovered transfer functions of the controllers and the plant models that we implemented in the PID Controller.


# Improvements
## Tuning
If we had more time we would have improved the performance of the tuned PID controller and completed more than 20 different combinations of PID control. This would have allowed us to get a response that wasn't underdamped and to optimize for the rise time more efficienctly. 

## Electronics
Maximum the Kp value in electronics, so that we are pulling the full capacity out of the hardware. If we use the generic DAQ we won't be able to maximize the rise time. For this we used an amplification circuit of 9/5 so that we can feed the full output range of the DAQ into the amplifier.

## Software
### Server Module
The server Module is the module that is responsible for maintaining the state of the controller

It will store remotely the exact temperature and the state of the controller

It uses a very lightweight REST API for the management and does all control in memory

It supports a single sensor module.

The digital control system will interact with this module and provide the functionality to the ios module. We implemented this module and have open-sourced this module, however never implemented it as the focus was on getting the PID module functional for the demonstration.

In the source there exists a matlab SDK for interacting with this remote server.


### iOS Module 
The iOS module is the UI/Interface module for the PID Controller. It provides a modern interface to the PID controller that falls under the general IOT unmbrella. 

It has the following functionality:
- Turn the controller on and off
- Modify the set-point
- Display the current temperature

The UI looks like:

!["iOS UI Screenshot not running"] [ios]

[circuit]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/initial_circuit.png "Initial Circuit Design"
[temp_cali]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/temp_calibration.jpg "Temperature Calibration"
[ios]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/ios_screenshot.png "iOS Application Screenshot"
[lab]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/lab_setup.jpg "Lab Setup"
[demo]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/demo.jpg "demo"
[tf]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/tf.png "Transfer Function"
[demo_long_term]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/demo_long_term.jpg "demo_long_term"
[demo_steady_state_zoomed]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/demo_steady_state_zoomed.jpg "demo_steady_state_zoomed"
[demo_steady_state_zoomed_marked]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/demo_steady_state_zoomed_marked.jpg "demo_steady_state_zoomed_marked"
[kp2kd10ki003]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/kp2kd10ki003.jpg "kp2kd10ki003"
[step_response]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/step_response.jpg "step_response"
[step_response_data_points]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/step_response_data_points.jpg "step_response_data_points"
[yesterdaykp2kd50ki003]: https://bitbucket.org/lukeexton/elec4123-pidcontroller/raw/master/images/yesterdaykp2kd50ki003.jpg "yesterdaykp2kd50ki003"


# Appendicies

## PID Controller
    %
    % ELEC4123
    % PID Controller
    %
    % Luke Exton and Rob Joseph © 2015
    %
    %
    
    close all
    clear all
    
    % DAQ Constants
    global VENDOR;
    VENDOR = 'ni';
    global DAQ_ID;
    DAQ_ID = 'nidaq';
    
    global DEVICE_NAME;
    info = daqhwinfo(DAQ_ID);
    if length(info.InstalledBoardIds)
    DEVICE_NAME = info.InstalledBoardIds{1};
    end
    
    % IO Constants
    global IN_CHANNEL;
    IN_CHANNEL = 0;
    global OUT_CHANNEL;
    OUT_CHANNEL = 0;
    global HIGH;
    HIGH = 5;
    global LOW;
    LOW = 0;
    
    % DAQ Sampling Details
    global SAMPLE_RATE;
    %SAMPLE_RATE = 2000;
    SAMPLE_RATE = 80; %TEST!!!!!!
    
    global SAMPLES_PER_TRIGGER
    % Total Number of samples we will take per trigger
    % There is nothing to suggest we need to change this number
    % Since we will modify it as we need it. 
    SAMPLES_PER_TRIGGER = 50; 
    
    global seconds;
    seconds = 300;
    
    %MAGIC GOES HERE
    global magic_seconds;
    magic_seconds = 5;
    
    
    % Intitialization of DAQ
    ai = analoginput(DAQ_ID, DEVICE_NAME);
    ao = analogoutput(DAQ_ID, DEVICE_NAME);
    addchannel(ai, IN_CHANNEL);
    addchannel(ao, OUT_CHANNEL);
    
    ai.SampleRate = SAMPLE_RATE;
    ai.SamplesPerTrigger = SAMPLES_PER_TRIGGER;
    ai.TriggerType = 'manual';
    
    start(ai)
    trigger(ai)
    wait(ai,10)
    
    % This is the initial voltage
    starting_voltage = mean(getdata(ai));
    
    
    % cutoff_voltage = 6.000;
    % if starting_voltage > cutoff_voltage
    %     msgID = 'TooHot:Error';
    %     msg = 'Temperature too hot.';
    %     %disp(starting_voltage);
    %     throw(MException(msgID,msg));
    % end
        
    % Step UP
    %putsample(ao,HIGH);
    
    
    % Iterate for n seconds
    data = [];
    data(1)=0;
    
    % This will be the maximum voltage to be output by this method.
    % This will be determined by the hardware itself. And should only be
    % reconfigured with adjustment to the amplification circuit above.
    U_MAX = 5;
    U_MIN = 2;
    setpoint=7.28;
    Kp=3;
    Ki=0.0763;
    Kd=10;
    x = [0,0,0];
    
    % Two value circular buffer.
    error = [0,0];
    % Implement the control mechanism
    controlling = true;
    
    % First iteration of dt
    dt = 1;
    
    % Iterate for n seconds
    data = [];
    data(1)=0;
    temp = [];
    Time=[];
    Time(1)=0;
    output_plot=[];
    output_plot(1)=0;
    error_plot=[];
    error_plot(1)=0;
    i=0;
    
    T=0;
    while controlling && (T < seconds)
        tic
        start(ai)
        trigger(ai)
        wait(ai, 10)
        i=i+1;
        
        % Get a current sample
        acquiredData = getdata(ai);
        data(end+1) = mean(acquiredData(SAMPLES_PER_TRIGGER)); %#ok<AGROW>
        
        % Calculate the current error between the current data
        % We take the mean of this since we are getting 50 points of data per
        error(1) = setpoint - mean(data(end));
        disp('e:');
        disp(error(1));
        % Proportional
        x(1) = error(1);
        % Integral
        x(2) = x(2) + error(1) * dt;
        % Derivative
        x(3) = ( error(1) - error(2) ) / dt;
        
        output = Kp * x(1) + Ki * x(2) + Kd * x(3);
        
        error(2) = error(1); % We can set this now that we are done with it.
        
        % Clipping design - No input greater than what the driver can handle
        % for the motor/generator combo
        %  if output > U_MAX
        %     output = U_MAX;  
        %  end   
        %  if output < U_MIN
        %     output = U_MIN;  
        %  end
        
    
        %  Clip output if it tries to go negative
        %  if output < 0
        %     disp('going negative')
        %     output = 0;  
        %  end
        
        %Display for debugging purposes
        %disp(T)
        %disp(error(1))
        %disp(data(end))
        disp('o:')
        disp(output)
        
        % Display data in real-time
        Time(i)=i/magic_seconds;
        figure(1)
        subplot(4,1,1)
        temp(end+1) = 2.929*data(end) + 23.674;
        plot(Time(1:i),temp(1:i));
        xlabel('time (sec)');
        ylabel('temperature(degrees Celsius)');
        title('controlled response');
        subplot(4,1,2)
        plot(Time(1:i),data(1:i));
        xlabel('time (sec)');
        ylabel('voltage diff(V)');
        title('controlled response');
        subplot(4,1,3)
        output_plot(end+1)=output;
        plot(Time(1:i),output_plot(1:i));
        xlabel('time (sec)');
        ylabel('PID output(V)');
        title('controlled response');
        subplot(4,1,4)
        error_plot(end+1)=error(1);
        plot(Time(1:i),error_plot(1:i));
        xlabel('time (sec)');
        ylabel('PID Input Error(V)');
        title('Error Plot');
        
        
        drawnow
        
        putsample(ao, output);
         dt = toc;
        T=T+toc;
    end   
    putsample(ao,LOW);
    
    %Commented out function bookends because wasn't executing
    %function plot_data (data)
        %function plot_data (data, smooth_data)
        figure(2)
        time = T/numel(data)*(1:(numel(data)));
        %plot(time, data, time, smooth_data)
        plot(time, data);
    
        xlabel('time (sec)');
        ylabel('voltage diff(V)');
        title('step response');
    %end
    
    
    %end

## Cooldown
    % DAQ Constants
    global VENDOR;
    VENDOR = 'ni';
    global DAQ_ID;
    DAQ_ID = 'nidaq';    
    global DEVICE_NAME;
    info = daqhwinfo(DAQ_ID);
    if length(info.InstalledBoardIds)
    DEVICE_NAME = info.InstalledBoardIds{1};
    end    
    % IO Constants
    global IN_CHANNEL;
    IN_CHANNEL = 0;
    global OUT_CHANNEL;
    OUT_CHANNEL = 0;
    global HIGH;
    HIGH = 3;
    global LOW;
    LOW = 0;    
    % DAQ Sampling Details
    global SAMPLE_RATE;
    SAMPLE_RATE = 2000;    
    global SAMPLES_PER_TRIGGER
    % Total Number of samples we will take per trigger
    % There is nothing to suggest we need to change this number
    % Since we will modify it as we need it. 
    SAMPLES_PER_TRIGGER = 50;     
    global seconds;
    seconds = 800;    
    %MAGIC GOES HERE
    global magic_seconds;
    magic_seconds = 5;        
    % Intitialization of DAQ
    ai = analoginput(DAQ_ID, DEVICE_NAME);
    ao = analogoutput(DAQ_ID, DEVICE_NAME);
    addchannel(ai, IN_CHANNEL);
    addchannel(ao, OUT_CHANNEL);    
    ai.SampleRate = SAMPLE_RATE;
    ai.SamplesPerTrigger = SAMPLES_PER_TRIGGER;
    ai.TriggerType = 'manual';    
    % Iterate for n seconds
    data = [];
    data(1)=0;
    temp = [];
    Time=[];
    i = 0;
    tic    
    while ((i < magic_seconds * seconds) && (data(end)<9.8)) %End safe & early
        i = i + 1;
        start(ai)
        trigger(ai)
        wait(ai,10)
        acquiredData = getdata(ai);
        data(end+1) = mean(acquiredData(SAMPLES_PER_TRIGGER));
        %Display time & data for debugging purposes
        disp(i/magic_seconds)
        disp(data(end))
        % Display data in real-time
        Time(i)=i/magic_seconds;
        figure(1)
        subplot(2,1,1)
        temp(end+1) = 2.929*data(end+1) + 23.674
        plot(Time(1:i),temp(1:i));
        subplot(2,1,2)
        plot(Time(1:i),data(1:i));
        xlabel('time (sec)');
        ylabel('voltage diff(V)');
        title('step response');
        drawnow
    end
    T = toc;
    figure(2)
    time = T/numel(data)*(1:(numel(data)));
    plot(time, data);
    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('step response');
    stabilized_samples = 100;
    stabilized_start = 5;
    three_db = 0.632;
    final_voltage = mean(data(end-stabilized_samples:end));
    starting_voltage = data(stabilized_start);
    voltage_diff = final_voltage - starting_voltage;
    crossing_point = starting_voltage + (three_db * voltage_diff);
    tau = 0;
    counter = 0;
    for v = data
        counter = counter + 1;
        if v > crossing_point
            tau = counter;
            break
        end
    end
    tau = tau / magic_seconds;
    a = 1/tau;
    k = a * voltage_diff;
    s = tf('s');
    % Since we are assuming single order
    % Validate against the measured step response.
    G = k / (s + a);
    H = (G + starting_voltage)/HIGH;
    figure(3);
    t=(0:1:1000);
    u=3*ones(size(t));
    lsim(H,u,t);     

## Generate Transfer Function
    %
    % ELEC4123
    % Transfer Function Generator
    %
    % Luke Exton and Rob Joseph © 2015
    %
    %
    
    %function [data] = generateTransferFunction
    
    close all
    clear all
    
    % DAQ Constants
    global VENDOR;
    VENDOR = 'ni';
    global DAQ_ID;
    DAQ_ID = 'nidaq';
    
    global DEVICE_NAME;
    info = daqhwinfo(DAQ_ID);
    if length(info.InstalledBoardIds)
    DEVICE_NAME = info.InstalledBoardIds{1};
    end
    
    % IO Constants
    global IN_CHANNEL;
    IN_CHANNEL = 0;
    global OUT_CHANNEL;
    OUT_CHANNEL = 0;
    global HIGH;
    HIGH = 3;
    global LOW;
    LOW = 0;
    
    % DAQ Sampling Details
    global SAMPLE_RATE;
    SAMPLE_RATE = 2000;
    
    global SAMPLES_PER_TRIGGER
    % Total Number of samples we will take per trigger
    % There is nothing to suggest we need to change this number
    % Since we will modify it as we need it. 
    SAMPLES_PER_TRIGGER = 50; 
    
    global seconds;
    seconds = 800;
    
    # this should be replaced with the T from the 
    # matlab internal functions,tic and toc.
    # 
    global magic_seconds;
    magic_seconds = 5;
    
    
    % Intitialization of DAQ
    ai = analoginput(DAQ_ID, DEVICE_NAME);
    ao = analogoutput(DAQ_ID, DEVICE_NAME);
    addchannel(ai, IN_CHANNEL);
    addchannel(ao, OUT_CHANNEL);
    
    ai.SampleRate = SAMPLE_RATE;
    ai.SamplesPerTrigger = SAMPLES_PER_TRIGGER;
    ai.TriggerType = 'manual';
    
    start(ai)
    trigger(ai)
    wait(ai,10)
    
    % This is the initial voltage
    starting_voltage = mean(getdata(ai));
    
    
    cutoff_voltage = 6.000;
    if starting_voltage > cutoff_voltage
        msgID = 'TooHot:Error';
        msg = 'Temperature too hot.';
        disp(starting_voltage);
        throw(MException(msgID,msg));
    end
        
    % Step UP
    putsample(ao,HIGH);
    
    
    % Iterate for n seconds
    data = [];
    data(1)=0;
    temp = [];
    Time=[];
    i = 0;
    tic
    
    while ((i < magic_seconds * seconds) && (data(end)<9.8)) %End safe & early
        i = i + 1;
        start(ai)
        trigger(ai)
        wait(ai,10)
        acquiredData = getdata(ai);
        data(end+1) = mean(acquiredData(SAMPLES_PER_TRIGGER));
        %Display time & data for debugging purposes
        disp(i/magic_seconds)
        disp(data(end))

        % Display data in real-time
        Time(i)=i/magic_seconds;
        figure(1)
	    subplot(2,1,1)
	    temp(end+1) = 2.929*data(end+1) + 23.674
	    plot(Time(1:i),temp(1:i));
        subplot(2,1,2)
        plot(Time(1:i),data(1:i));
        xlabel('time (sec)');
        ylabel('voltage diff(V)');
        title('step response');
        drawnow
    end
    T = toc;
    putsample(ao,LOW);
    
    % Filter out the data that we are processing
    % This might help with the reduction of noise
    % It is noted that this wasn't as neccisary once we
    % Decoupled the power supply and had a low-noise
    % Segment of the breadboard
    
    filter_order = 1;
    cutoff = 25;
    [b,a]=butter(filter_order,2*cutoff/SAMPLE_RATE);
    smooth_data = filter(b,a,data);
    
    figure(2)
    time = T/numel(data)*(1:(numel(data)));
    plot(time, data);
    
    xlabel('time (sec)');
    ylabel('voltage diff(V)');
    title('step response');
    
    % Analyize the data set to calculate the 3db sample point.
    % Currently assumes we have stability for 100 samples at least.    
    
    % Constants
    stabilized_samples = 100;
    stabilized_start = 5;
    three_db = 0.632;
    
        % Measured Data
    final_voltage = mean(data(end-stabilized_samples:end));
    starting_voltage = data(stabilized_start);
    
    voltage_diff = final_voltage - starting_voltage;
    crossing_point = starting_voltage + (three_db * voltage_diff);
    
    % Identify the time in seconds of the crossing point.
    % This consumes the magic number to convert the 
    % sampling rate back to seconds rather than per sample.
    
    tau = 0;
    counter = 0;
    for v = data
        counter = counter + 1;
        if v > crossing_point
            tau = counter;
            break
        end
    end
    % Magic Conversion of the samples to time
    tau = tau / magic_seconds;

     a = 1/tau;
     k = a * voltage_diff;
     s = tf('s');
 
     % Since we are assuming single order
     % Validate against the measured step response.
     G = k / (s + a);
     H = (G + starting_voltage)/HIGH;
     
     % Make sure we plot this on a different figure to the previous
     figure(3);
     t=(0:1:1000);
     u=3*ones(size(t));
     lsim(H,u,t);
     
    %end